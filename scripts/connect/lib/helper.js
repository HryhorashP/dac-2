var _ = require('lodash');
var http = require('request');

module.exports = {
  downloadData(url) {
    return new Promise((resolve, reject) => {
      http.get(url, (err, response, body) => {
        if (err) {
          reject(err);
        } else {
          resolve(body);
        }
      });
    });
  },
  findIndexOf(list, key, val) {
    return _.findIndex(list, (o) => {
      return o[key].toLowerCase() == val.toLowerCase();
    });
  }
}