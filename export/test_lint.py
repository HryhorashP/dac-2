import lint


def test_url_matcher_trailing_paren_dot():
    node = "//Try https://nodejs.org. Also (http://nodejs.org)."
    links = lint.collect_links(node)
    assert len(links) == 2
    assert links[0] == "https://nodejs.org"
    assert links[1] == "http://nodejs.org"


def test_url_matcher_trailing_junk():
    node = "http://nodejs.org\" http://nodejs.org> http://nodejs.org<a href="
    links = lint.collect_links(node)
    assert len(links) == 3
    assert links[0] == "http://nodejs.org"
    assert links[1] == "http://nodejs.org"
    assert links[2] == "http://nodejs.org"


def test_various_trailing():
    chars = ['>', "#", "<", "\"", "'", ")", ".", "[", "]", "(", "`"]
    for c in chars:
        links = lint.collect_links("http://foobar.com%s" % c)
        assert len(links) == 1
        assert links[0] == "http://foobar.com"


def test_url_matcher_only_http():
    """
    While we want to support schemaless URLs (aliasing to http or https) we do not want to support all
    schemas.
    """
    text = "  node2     -        digitalocean   Running   tcp://178.62.212.73:2376    cluster"
    assert len(lint.collect_links(text)) == 0


def test_schemaless():
    links = lint.collect_links("check this //foobar.com")
    assert len(links) == 1
    assert links[0] == "http://foobar.com"


def test_two_close_by():
    this_happened = "https://ecosystem.atlassian.net/browse/AC](https://ecosystem.atlassian.net/browse/AC "
    links = lint.collect_links(this_happened)
    assert len(links) == 2
    assert links[0] == "https://ecosystem.atlassian.net/browse/AC"
    assert links[1] == "https://ecosystem.atlassian.net/browse/AC"


def test_blacklisting():
    assert not lint.ok_to_check("http://localhost/")
    assert not lint.ok_to_check("http://localhost:8080/")
    assert not lint.ok_to_check("http://127.0.0.1")
    assert not lint.ok_to_check("http://127.0.0.1:1234")
    assert not lint.ok_to_check("http://0.0.0.0")
    assert not lint.ok_to_check("https://marketplace.atlassian.com/rest/2")
    assert not lint.ok_to_check("http://0.0.0.0:8000")
    assert not lint.ok_to_check("http://HOSTNAME.atlassian.net")
