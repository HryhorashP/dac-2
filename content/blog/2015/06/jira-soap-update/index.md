---
title: "JIRA SOAP API removal update"
date: "2015-06-26T15:00:00+07:00"
author: "dmeyer"
categories: ["JIRA","SDK","APIs"]
---

This is a follow up to our [previous reminder](https://developer.atlassian.com/blog/2015/05/jira-soap-api-announcement?utm_source=dac&utm_medium=blog&utm_campaign=jira-soap-api)
 about the JIRA SOAP API. The JIRA SOAP API will be removed from JIRA Cloud on July 6.

Once more, in big letters:

## The JIRA SOAP API will be removed from JIRA Cloud on July 6 2015.

After our next JIRA Cloud release on July 6, JIRA's SOAP and XML-RPC APIs will no longer be available,
and any integrations making requests to those APIs will not behave correctly.

We have collaborated with several add-on developers, including Zephyr and ServiceRocket,
to make sure that popular integrations continue to work seamlessly.
However, if you have developed a custom integration that uses the JIRA SOAP API,
we recommend you migrate it as soon as possible.

[Get started with the JIRA SOAP to REST Migration Guide](https://developer.atlassian.com/jiradev/jira-apis/jira-api-status/jira-rpc-services/jira-soap-to-rest-migration-guide?utm_source=dac&utm_medium=blog&utm_campaign=jira-soap-api).

If you are running JIRA on your own server, the SOAP API will be removed with the 7.0 release of JIRA Server.

We strongly encourage you to collaborate with us if you need help, need to report a bug,
or have a feature request for the JIRA Ecosystem team
simply [create an issue in the ACJIRA project on ecosystem.atlassian.net](https://ecosystem.atlassian.net/secure/CreateIssue!default.jspa?selectedProjectId=17771).
