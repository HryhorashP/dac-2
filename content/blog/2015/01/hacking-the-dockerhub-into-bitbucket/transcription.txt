Hello, everyone. My name is Nicola Paolucci. I am Developer Instigator at
Atlassian. Today, I want to tell you about the results of hackathon, or as we
call it the ShipIt project.

It's fun to share and you'll find interesting. For those that don't know, at
Atlassian we have a big culture of innovation and experimentation. Every
quarter, the company stops for 24 hours and anybody can pick their own project
to scratch their own itch, form groups, sprint and spike new ideas to solve our
own problems.

It can be things completely off the wall. It can be tiny improvements. It can
be something very innovating, very fun. Several very important features that
have been included in our products came out of this sort of sprints of
innovations.

That was very exciting time for me. Sometime ago, I was in San Francisco. I
paired up with a couple of colleagues. This is what I wanted to work on. Guys,
no, you might know, or not know, I have been very excited about Docker. I have
been talking about Docker a lot.

There is one thing that I thought will be easy to do. I got to work. My idea
was, the Docker registry collects a lot of software packages or images
ready-made, ready to be used. We already have quite a cool integration with the
Docker registry in the sense that you can set-up the Docker registry to rebuild
your image anytime you push something to Bitbucket.

The integration from Bitbucket to the Docker registry is already in place. What
I thought about was, would it have been nice if we could show the state of the
images and also the interaction with the Docker community in Bitbucket? That
seems something relatively easy to do and also a piece useful integration. I
brainstorm early a bit how to do it.

What I realized, that I needed was...I thought I needed a way to be the very
simple, static website that only base on the HTML, JavaScript and CSS.
Recently, I've got very excited about React JS. I wanted to use that bit of
technology as I had to host the static website somewhere.

I thought to use Nginx. We only concerned that we'll avoid like Cross Origin
Resource Sharing problems. I needed to make sure that my request to the Docker
Hub would be proxied. I had to set-up Nginx to do that.

Obviously, I wanted to deploy the application using Docker, deploy this hack
into a container and also update it, making sure that it could update it using
containers too. That's what I did. That's what we did.

Before I go on, maybe you guys are not familiar with some of the technologies I
mentioned. Let me give you a very brief high level description of what I used.
The first bit of framework that I used is React. It's a JavaScript framework,
developed inside Facebook, now open-sourced. That has a very cool unique
characteristics, especially the way that components are connected together.

It reduces the boilerplate and makes reasoning about your application much
easier. They've explained that it is like the V in the MVC framework. If you
have been following a little bit the developments in this area, when Facebook
guys have come up with actually a new paradigm for designing user interfaces
and Web publications that they call Flux.

What Flux is is basically a way to transport events through an application
which is unidirectional. By constraining this direction on messages are passed
through the application, reasoning about your application becomes very easy,
getting on board the people becomes very easy.

For me, it has been a quite a pleasure starting this framework and getting to
work with it. That's one bit of technology that I've used.

The second one is obviously the Docker set-up for it. As I told you, I only
needed a set of static files deployed somewhere on a Web server. When you have
a look at my Dockerfile, you'll see that it's very simple. I just pull the
Nginx image to the official Nginx image. Copy the certificates because I had to
use https because Bitbucket requires everything to be https.

I need to add my own configuration for the proxy set-up, which I'll show just
afterwards. Then I had to just add the source code, the source files, the
JavaScript files, the HTML files that I wanted the Nginx to server.

To solve the CORS problems, I just need it to make sure that I could query the
Docker Hub APIs through a proxy so that JavaScript would think that it would
call the local host. This is the relevant part of my Nginx configuration file.
With all these set-up, I'm ready to go. I know what you guys are thinking. "Get
to work already. Come on, show me what you did."

I have another one for you and it says like, "Do you guys recognize this one?"
It says, "Where is my Demo Lebowski? Praying to the Demo Gods, hopefully now I
can show where that came up. Let's start from the end.

Go to Bitbucket and refresh. I show you a page that contains a Dockerfile and
it has the same name on the Docker Hub. What you just saw appearing is this
small static website that I've created, that is now plugged into the project
overview page of Bitbucket. Now you might say, "Is this live on Bitbucket?"
Yes, it is.

What I did? I worked with some of the Bitbucket guys. We agreed to open a small
I-frame window in there where I could plug my code. It is live right now, but
it is obviously only turn on for a group of users internal to Bitbucket. We'll
see what's going to happen of this feature in the future. For now, it's just a
hack.

I showed you that it's actually working. What I wanted to do is actually go one
step forward and show you that I can do a live update of this using Docker
containers. I'll switch to the command line and we'll go through the process
together.

If I do Docker images, you will see that I have built an image called
durdn/bithub. I call this very creatively, bithub. What I wanted to do is I'll
do a live-go change, reveal the total of my local image, push the image, and
then we deploy it using fig.

If I go to source, and I know that there is a component that I need to change
which is like the Docker images one. I want to just change some text so that I
could show you that we do a live deployments that we do. I go...let's do it
like this. "Hello Gods," and I save it.

Now, what I can do is just rebuild my local image. I want to make sure what the
build command contains. The build command is very simple. It just regenerates
the entire JavaScript of the application, stores it in a file name called
bundle.js. Then I rebuild the application using the Dockerfile.

The Dockerfile is very simple. It just copies the content of the source folder,
add my certificates, and the proxy set-up for Nginx. In this case, rebuild the
image only at the last step, it should happen. I rebuild the image now. What
will happen? A new image will be created. A tag will do to let us the tag.

The build script also pushes this new image to the index. Right now, the bithub
one is a private image. After that's done, we'll update it on the live server.
There are many ways obviously to handle this. I just hack together a script
that allows me to re-deploy on my small Docker box an image, and get it up and
running.

I'm pushing a new version of the image. After that's done, we'll update it on
the live server and see what happens. Give me a few more minutes. Give me a few
more seconds, hopefully. Because right now I am Sydney and the server that I
started up is in Amsterdam, there is a slight delay.

Generally, it is quite fast. Let it go. I'll see if the Demo Gods have been
kind to me. The push is still happening. Hopefully soon.

I might cut this part of the video. Buffering to disk. Now it is actually
pushing the updates, pushing the tag. On the Docker index, we have a new
version of image of my application. I want to redeploy it, what I cook up
together is I have box running with Docker running.

I created a very simple fig file, which I can show you, it is very simple. It
just says I want to run the durdn/bithub application and open two ports -- the
port 80 and the port 443. My deployment script is incredibly simple. What it
does is, it just pulls the latest Web image, it stops the previous one, and
restart the containers in daemon mode so that they stay up in the background.

Let's do it and see what happens. These commands mean run remotely on this sh
switch box that I have access to. New image is being pulled, new update to the
application is being pulled, and the application is restarted.

If everything goes well, by refreshing the page we should have now a live
deployment on production, on Bitbucket for your pleasure to view. Let me try
it, let me see it. Let's see if this work. I'm refreshing the page, crossing my
fingers.

We have redeployed live on Bitbucket, container rise a static site, and that
actually gets the information from the Docker Hub into Bitbucket. I was very
pleased to get the results of this. Obviously, it's a tiny hack, but I hope you
guys found this useful. If you have feedback or you like it, you Twitter me at
durdn. I'll be happy to know what you guys think about it.

Thank you very much for listening. See you next time. Bye.
