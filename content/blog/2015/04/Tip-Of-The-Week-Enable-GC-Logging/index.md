---
title: "Tip of the Week: Enable Garbage Collection Logging"
date: "2015-04-20T16:00:00+07:00"
author: "jgarcia"
categories: ["Java", "Server"]
---

Forewarned is forearmed, or so the saying goes. This means in the event of an
incident, it's always best to have as much logging information available for
diagnosis and remediation as is reasonable.

Garbage collection is a crucial part of keeping my JVM happy, so I like to get
garbage collection logging enabled before I need it rather than after. In the
pattern used by Atlassian's Java apps, we will enable logging using the
setenv.sh file, in the `JVM_SUPPORT_RECOMMENDED_ARGS` variable. Use the
following context:

    -XX:+PrintGC -XX:+PrintGCDetails -XX:+PrintGCTimeStamps -Xloggc:<file>

If you prefer to have actual system datestamps logged instead of millis offset
from VM Launch, use `-XX:+PrintGCDateStamps` instead of
`-XX:+PrintGCTimeStamps`.

This will set up your system to log every garbage collection run into a text file
that will be generated in your `<product-install>/logs` folder for easy
retrieval. This file will be destroyed and a new one generated everytime you
start the JVM, so you'll want to retrieve it beforehand if you need to restart
the application.

Garbage collection tuning is as much an art as a science. For more advice on how
to tune garbage collection in Stash, check out this page: [Garbage Collection
Tuning](https://confluence.atlassian.com/display/ENTERPRISE/Garbage+Collection+%28GC%29+Tuning+Guide).
The guidelines found at that page are suitable for any tuning project.

Watch out for news and info from our [@AtlassianDev](https://www.twitter.com/atlassiandev) Twitter feed!

Follow me at [@bitbucketeer](https://www.twitter.com/bitbucketeer)!
