---
title: "Tip of the Week - Use JIRA Agile boards to visualize anything"
date: "2015-10-02T16:00:00+07:00"
author: "pvandevoorde"
categories: ["totw","JIRA"]
lede: "JIRA Agile can be used for more then just visualizing your development workflow.
You could try managing your beer or wine collection. Or plan your wedding or the arrival of a baby.
Take a look at some of our examples and share yours!"
---
This week's article is about using JIRA Agile to visualize much more then just your development workflow.
As developers we are familiar with using JIRA Agile to track our tasks, bugs, stories and epics in an easy
and intuitive way. We might even use it to track the tasks of several teams across the company on one big board.
But did you know that you could pretty much visualize anything you wanted using JIRA Agile?
Once you understand the basics of the JIRA workflow and issue types you can do amazing things like:

* track books and articles you want to read
* track conference sessions you want follow in real-life or watch later online
* track certifications you want to obtain
* manage your game collection
* and many more...

Here are some of the crazy things we at Atlassian have done using JIRA Agile:

* Sarah Goff-Dupont explains how you can
<a href="http://blogs.atlassian.com/2014/05/life-runs-jira-sommelier-edition/?utm_source=dac&utm_medium=blog&utm_campaign=totw">manage your wine collection</a>
* Sarah loves it so much she even used it to
<a href="http://blogs.atlassian.com/2013/11/my-life-runs-on-jira-project-little-man/?utm_source=dac&utm_medium=blog&utm_campaign=totw"> prepare for the arrival of her second child </a>
* I once blogged about using it for
<a href="http://www.petervandevoorde.com/2013/02/26/project-portfolio-management-with-jira-and-greenhopper/?utm_source=dac&utm_medium=blog&utm_campaign=totw">
 doing light weight PPM</a>

* Mark Halvorson even uses it to track his <a href="http://blogs.atlassian.com/2015/01/fun-fridays-track-new-years-resolutions-jira/?utm_source=dac&utm_medium=blog&utm_campaign=totw"> new years resolutions</a>:<br>
<br>
<center><iframe width="600" height="368" src="https://www.youtube.com/embed/UhhecKzdIoY" frameborder="0" allowfullscreen></iframe></center>
<br>
So what are your cool uses of JIRA Agile? Share them in the comments!
