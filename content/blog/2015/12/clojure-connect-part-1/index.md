---
title: "Let's build a Bitbucket Add-On in Clojure! - Part 1: Building a web stack"
date: "2015-12-02T06:00:00+07:00"
author: "ssmith"
categories: ["bitbucket", "atlassian-connect", "clojure", "bbclojureseries"]
---

<style>
  .float-image {
      display: block; 
      margin: 15px auto 30px auto;
  }
  .shadow-image {
      display: block; 
      margin: 15px auto 30px auto;
      box-shadow: 10px 10px 15px #888888;
  }
</style>

One the most exciting things about the
[Atlassian Connect add-on framework][connect-intro], for me at least, is that it
removes the need to create add-ons in the language of the hosting application.
With the [recent release][bb-connect-announce] of Bitbucket support for Connect
we now have the ability to not-only extend Bitbucket in any way we see fit, but
to also do it in whatever language or framework we desire. This opens us up to
developing for Atlassian products in Haskell, Scala, Node.js, or anything else
that supports the basic protocols of the web.

Personally I'm rather fond of [Clojure], so when we wanted to produce a
[Docker Hub add-on for Bitbucket][docker-hub-announce] that was the technology I
chose. But as this was the first Bitbucket Connect add-on written in Clojure I
chose to port [Tim Pettersen's][tpettersen]
[example NodeJS Connect project][bb-node-example] first to work out any issues.
This turned out to be a very useful process, so I thought I'd share it here in a
series of posts.

*Note*: To streamline things I'm going to assume you already have basic
knowledge of Clojure development. In particular I'll assume you already have a
development environment of choice; this allows me to skip the details of working
with Emacs/[Cider][Cider], Vim/[Fireplace], [La Clojure], [Cursive], etc. I'll
leave that up to you. But other than that I'll introduce the fundamental
components of a Clojure web-stack from the ground up.
 

# Anatomy of a Connect Add-On

<img class="float-image" src="architecture.png" />

I won't reiterate all the details of how Connect works, but in essence the
diagram shows that a Connect add-on is a web application written in any language
you like, running on any stack you choose, in any location you want. Once
registered with the Atlassian application your web app can progressively enhance
ours with new screens, features and functions that appear directly embedded as
if a part of our cloud. It can also enhance our app with new behind-the-scenes
logic, all via REST APIs and Webhooks. All of Connect's components consist of
standard web protocols and conventions, and, apart from a
[minor extension][JWT-qsh] to the [JWT] spec, are supported by most languages
and libraries out of the box. This is part of the power of Connect, and what
gives it a true cross-platform experience. You can read
<a href="https://developer.atlassian.com/bitbucket/guides/introduction.html" target="_blank">more details of the Connect architecture here</a>.


# Let's get started

The first thing we need to define is our project properties, such as versioning,
build structure, and dependencies. Clojure now has two competing yet
complementary project tools: [Leiningen] and [Boot]. They take fundamentally
different approaches to defining a project, and both (to me) have their
attractions and use-cases. I'm going to use Leiningen for this project as it's
the one I'm most familiar with, and it has better support for [Maven]-style
project versioning, but Boot works just as well if you prefer it.

I'll assume you have the latest [JDK] installed along with the `lein` command on
your path. If not, follow the [appropriate][jdk-install]
[instructions][Leiningen] for getting these on your platform of choice.

The first step is to have Leiningen create our project for us. Leiningen has a
number of pre-made templates for web-based projects that will setup all the
necessary dependencies and structures, the most popular being [Luminus]. However
full-featured "frameworks" that exist in other languages (such as Ruby on Rails,
or Django) are not really part of the Clojure experience. The Clojure community
prefers to build small, [composable] components that can be combined to provide
framework-like functionality as needed. Even Luminus works this way; it's really
just a ready-made combination of the current best-of-breed web components from
elsewhere.

So in the true Clojure fashion, let's create a default project and construct our
Connect add-on from the bottom-up...

````shell
[ssmith:~/projects] $ lein new app hello-connect
Generating a project called hello-connect based on the 'app' template.
[ssmith:~/projects] $ cd hello-connect/
[ssmith:~/projects/hello-connect] $ ls -al
total 32
drwxr-xr-x 12 ssmith staff   408 Nov 18 13:57 .
drwxr-xr-x  9 ssmith staff   306 Nov 18 13:57 ..
-rw-r--r--  1 ssmith staff    99 Nov 18 13:57 .gitignore
-rw-r--r--  1 ssmith staff   122 Nov 18 13:57 .hgignore
-rw-r--r--  1 ssmith staff   792 Nov 18 13:57 CHANGELOG.md
-rw-r--r--  1 ssmith staff 11218 Nov 18 13:57 LICENSE
-rw-r--r--  1 ssmith staff   239 Nov 18 13:57 README.md
drwxr-xr-x  3 ssmith staff   102 Nov 18 13:57 doc
-rw-r--r--  1 ssmith staff   273 Nov 18 13:57 project.clj
drwxr-xr-x  2 ssmith staff    68 Nov 18 13:57 resources
drwxr-xr-x  3 ssmith staff   102 Nov 18 13:57 src
drwxr-xr-x  3 ssmith staff   102 Nov 18 13:57 test
[ssmith:~/projects/hello-connect] $
````

We should also add version-control to our project by doing `git init` and doing
a initial add/commit. It is also good practice to create a remote repository for
backup; obviously my preferred one is [Bitbucket].


## Setting up project.clj

Leiningen produces a lot of boiler-plate files for us. The main one of interest
at this point is `project.clj`; this is the Leiningen project definition file.
This is pretty basic at the moment:

````clojure
(defproject hello-connect "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
   :dependencies [[org.clojure/clojure "1.7.0"]]
   :main ^:skip-aot hello-connect.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
````
 Feel free to fill in the
metadata as you see fit. And then we can start adding dependencies.

### Quick Tip: Automatic versions 

As you can see from the `org.clojure/clojure` entry above Leiningen dependencies
use Maven-style group/artifact/version specifications (in-fact Leiningen and
Boot both reuse Maven's existing infrastructure). Normally we would now need to
go off and find out the required versions of all our libraries and add them in
here. However as we will generally use the latest version, we can short-cut this
using [lein ancient], a plugin to check your project for outdated dependencies
and plugins. So the first thing we'll do is add this to our project:

    :plugins [[lein-ancient "0.6.8"]]

Once we've added our dependencies we'll use this to fill in the blanks.

### Adding HTTP/REST support

The first dependency we'll add is [clj-connect]. This is a wrapper around
`clj-jwt` that supports the [Atlassian JWT extensions][JWT-qsh] and defines some
common operations for Connect add-ons, such as token retrieval:

````clojure
[clj-connect]
````

As Connect add-ons operate as HTTP REST servers we'll also add dependencies on
the standard `ring-*`/`compojure` Clojure HTTP stack. [Compojure] and [Ring]
have some REST add-ons that support advanced tooling such as [Swagger], but as
we're not defining public API this is overkill, so we'll stick to the core APIs
and add the following to our dependency list:
    
````clojure
[ring]
[ring/ring-defaults]
[ring/ring-json]
[ring.middleware.logger]
[compojure]
````

### Web server

The preferred way to build small scalable web applications is to follow the
[12 Factor] guidelines. This means we'd like our add-on to be self-contained and
serve its own content, which means embedding a HTTP server into the app. As
Clojure is built on top of Java there are a number of embedded HTTP engines
available, not least the venerable Aussie workhorse [Jetty]. However the Clojure
web community is increasingly coalescing around the [Immutant] web module (which
is build on top of [Undertow]), especially since version 2 when it became much
more modular:

````clojure
[org.immutant/web]
````

As a bonus the latest versions of Immutant/Undertow support [HTTP/2]. Immutant
will require some manual invocation, which we will set-up in a bit.

### Our first build

Our `project.clj` now looks like:

````clojure
(defproject hello-connect "0.1.0-SNAPSHOT"
  :description "A demo Connect Addon"
  :url "https://bitbucket.org/ssmith/hello-connect/"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :plugins [[lein-ancient "0.6.8"]]

  :dependencies [[org.clojure/clojure "1.7.0"]

                 [clj-connect]

                 [ring]
                 [ring/ring-defaults]
                 [ring/ring-json]
                 [ring.middleware.logger]
                 [compojure]
                 [org.immutant/web]]

  :main ^:skip-aot hello-connect.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
````
  
We can now get `lein ancient` to fill-in the latest versions for us. Ancient is
quite conservative and will run your tests after updating the dependencies and
revert if they fail. However Leiningen drops a dummy _failing_ test in the
`test` directory, so this will never work. It's possible to tell `ancient` to
ignore the tests, but for now we can just delete the fake test:

    [ssmith:~/projects/hello-connect] $ rm test/hello_connect/core_test.clj

Now we run `ancient`:

    [ssmith:~/projects/hello-connect] $ lein ancient upgrade
    [clj-connect "0.2.1"] is available but we use ""
    [ring "1.4.0"] is available but we use ""
    [ring/ring-defaults "0.1.5"] is available but we use ""
    [ring/ring-json "0.4.0"] is available but we use ""
    [ring.middleware.logger "0.5.0"] is available but we use ""
    [compojure "1.4.0"] is available but we use ""
    [org.immutant/web "2.1.1"] is available but we use ""

    running test task ["test"] ...

    lein test user

    Ran 0 tests containing 0 assertions.
    0 failures, 0 errors.
    6/6 artifacts were upgraded.

And now our `project.clj` looks like:

````clojure
(defproject hello-connect "0.1.0-SNAPSHOT"
  :description "A demo Connect Addon"
  :url "https://bitbucket.org/ssmith/hello-connect/"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}

  :plugins [[lein-ancient "0.6.8"]]

  :dependencies [[org.clojure/clojure "1.7.0"]

                 [clj-connect "0.2.1"]

                 [ring "1.4.0"]
                 [ring/ring-defaults "0.1.5"]
                 [ring/ring-json "0.4.0"]
                 [ring.middleware.logger "0.5.0"]
                 [compojure "1.4.0"]

                 [org.immutant/web "2.1.1"]]

  :main ^:skip-aot hello-connect.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
````
  
You can also run `ancient` in interactive mode with `lein ancient :interactive`,
which allows you to cherry-pick the updates you want.

Now we can have Leiningen fetch the dependencies for us:

    [ssmith:~/projects/hello-connect] $ lein deps
    Retrieving clj-connect/clj-connect/0.2.1/clj-connect-0.2.1.pom from clojars
    Retrieving ring/ring-codec/1.0.0/ring-codec-1.0.0.pom from clojars
    .
    .
    .
    Retrieving org/immutant/web/2.1.1/web-2.1.1.jar from clojars
    Retrieving org/immutant/core/2.1.1/core-2.1.1.jar from clojars

## Creating a runnable app

Now that we have our necessary dependencies we can get started on making our
project into an actual working app. The first thing we'll need is a set of
skeleton [Ring] routes. We'll add this into a new namespace called `handler` in
`src/hello_connect/handler.clj`:

````clojure
(ns hello-connect.handler
  (:require [clojure.tools.logging :as log]

            [ring.util.response :as response]
            [ring.middleware.defaults :refer [wrap-defaults site-defaults]]
            [compojure.core :refer :all]
            [compojure.route :as route]))

(defroutes app-routes
  (GET  "/" [] "Hello Connect!")
  (route/not-found {:status 404 :body "Not Found"}))


(def app
  ;; Disable anti-forgery as it interferes with Connect POSTs
  (let [connect-defaults (-> site-defaults
                             (assoc-in [:security :anti-forgery] false)
                             (assoc-in [:security :frame-options] false)) ]

    (-> app-routes
        (wrap-defaults connect-defaults))))

(defn init []
  (log/info "Initialising application"))

(defn shutdown []
  (log/info "Shutting down application"))
````
  
This defines three things; a set of routes called `app`, and two hooks to be
called on startup and shutdown. Currently the only route setup is the root "/"
one, which just returns dummy text. We'll expand on these later, primarily by
adding entries to the `defroutes` section.

While this adds the skeleton app structure we still need an Immutant HTTP server
for it to live in. To do this we'll use the existing `core.clj` namespace that
was created by Leiningen; it contains a placeholder `(-main)` function but we'll
replace that with the necessary Immutant/Undertow invocations:

````clojure
(ns bb-example.core
  (:require [clojure.tools.logging :as log]

            [bb-example.handler :as handler]

            [immutant.web :as web]
            [immutant.web.middleware :refer [wrap-development]]

            [environ.core :refer [env]]))

(defonce server (atom nil))
(defonce port (env :port 3000))

(defn start-server [port]
  (handler/init)
  (reset! server (web/run (wrap-development handler/app) :port port)))

(defn stop-server []
  (when @server
    (handler/shutdown)
    (web/stop @server)
    (reset! server nil)))

(defn start-app []
  (.addShutdownHook (Runtime/getRuntime) (Thread. stop-server))
  (start-server port)
  (log/info "server started on port:" (:port @server)))

(defn -main [& args]
  (start-app))
````

We use the `wrap-development` middleware here as this gives us useful things
such as auto-reloading of our code which helps with dynamic development.

One new library I'm introducing here is `environ`, which gives us convenient
access to environment variables, in this case `PORT`. The practice of defining
all our runtime configuration in the system environment is part of the
[12 Factor] philosophy. We'll be using this technique extensively in the
following installments.

Note that I haven't explicitly added this to the dependencies; from this point
I'll take updating these as a given. If in any doubt about where a library comes
from please refer the final version in Bitbucket:

https://bitbucket.org/ssmith/hello-connect/src/HEAD/project.clj

So we can now take our server for spin; we can do this by invoking `(-main)`
from our development REPL if we're using one, or via Leiningen:

    [ssmith:~/projects/hello-connect] $ lein run
    <snip logging info>
    13:38:00.602 INFO  [org.projectodd.wunderboss.web.Web] (main) Registered web context /
    13:38:00.607 INFO  [hello-connect.core] (main) server started on port: 3000

Now if you visit `http://localhost:3000` you should see our "Hello Connect!"
message.

# That's a wrap on part 1

Now we have the basic server up and running that's probably a good point to take
a break. So far nothing we've done is specific to Atlassian Connect. That will
change in the next installment when we introduce the
[Connect descriptor][descriptor-doc] and look at some of the options for
templating and routing requests.

This section of the tutorial has introduced a lot of Clojure technologies. While
I've sought to justify my decisions I'm always interested in hearing about how
others use Clojure in practice. Feel free to add your tips in the comments.

See you next time.


[bb-connect-announce]: https://blog.bitbucket.org/2015/06/10/atlassian-connect-for-bitbucket-a-new-way-to-extend-your-workflow-in-the-cloud/
[connect-intro]: https://developer.atlassian.com/bitbucket/guides/introduction.html
[JWT]: https://en.wikipedia.org/wiki/JSON_Web_Token
[JWT-qsh]: https://developer.atlassian.com/bitbucket/concepts/understanding-jwt.html
[clj-connect]: https://bitbucket.org/ssmith/clj-connect
[Leiningen]: http://leiningen.org/
[Boot]: http://boot-clj.com/
[Maven]: https://maven.apache.org/
[JDK]: http://www.oracle.com/technetwork/java/javase/downloads/index.html
[Luminus]: http://www.luminusweb.net/
[composable]: https://en.wikipedia.org/wiki/Composability
[lein ancient]: https://github.com/xsc/lein-ancient
[Compojure]: https://github.com/weavejester/compojure
[Ring]: https://github.com/ring-clojure/ring
[Swagger]: http://swagger.io
[12 Factor]: http://12factor.net/
[Jetty]: http://www.eclipse.org/jetty/
[Immutant]: http://immutant.org/
[Undertow]: http://undertow.io/
[s-expressions]: https://en.wikipedia.org/wiki/S-expression
[Handlebars]: http://handlebarsjs.com/
[ERB]: http://apidock.com/ruby/ERB
[Selmer]: https://github.com/yogthos/Selmer
[Django]: https://www.djangoproject.com/
[ngrok]: https://ngrok.com/
[bb-start]: https://developer.atlassian.com/bitbucket/guides/getting-started.html
[lein-profiles]: https://github.com/technomancy/leiningen/blob/master/doc/PROFILES.md
[storage.clj]: https://bitbucket.org/ssmith/bitbucket-docker-connect/src/HEAD/src/clojure/docker_connect/storage.clj?at=master
[EDN]: https://github.com/edn-format/edn
[clojure-json]: https://github.com/clojure/data.json
[ring-json]: https://github.com/ring-clojure/ring-json
[clj-jwt]: https://github.com/liquidz/clj-jwt
[bb-docker-connect]: https://bitbucket.org/ssmith/bitbucket-docker-connect
[Bitbucket]: https://bitbucket.org/
[Clojure]: http://clojure.org/
[docker-hub-announce]: https://developer.atlassian.com/blog/2015/09/docker-bitbucket/
[tpettersen]: https://developer.atlassian.com/blog/authors/tpettersen/
[bb-node-example]: https://developer.atlassian.com/bitbucket/guides/getting-started.html
[Cider]: https://github.com/clojure-emacs/cider
[Fireplace]: https://github.com/tpope/vim-fireplace
[La Clojure]: https://plugins.jetbrains.com/plugin/4050
[Cursive]: https://cursiveclojure.com/
[descriptor-doc]: https://developer.atlassian.com/bitbucket/descriptor/
[HTTP/2]: https://http2.github.io/
[jdk-install]: http://www.oracle.com/technetwork/java/javase/downloads/index.html
