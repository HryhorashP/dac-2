---
title: "Changes to comments"
date: "2015-12-03T15:00:00+07:00"
author: "tsmith"
categories: ["Developer experience"]
---

Earlier this year, [we moved comments on confluence.atlassian.com][comments-on-cac] to [Atlassian Answers],
and now we're moving comments from documentation pages on developer.atlassian.com to match.
We will provide links and integrations with Atlassian Answers to give you a place to ask questions and get support.
Disqus comments on blog posts, such as this post, will remain.

A preview of what you can expect to see next week:

<img style="display:block;margin: 0 auto;" src="aac-widget.png" title="preview of Atlassian Answers integrations" />

# Why are we doing this?

We believe that this will help increase the response rate on comments that appear on developer.atlassian.com.
Often comments go without response, and the teams with the right knowledge to respond
don't engage &mdash; but Atlassian Answers has a positive rate for responses to questions.
Teams in Atlassian and around our ecosystem already answer questions on Atlassian Answers.

Comments that revolve around code can have a wider context than a single page, and Atlassian Answers
provides a platform that makes that more obvious.
Access and visibility to great questions and responses automatically happens, great content isn't
lost as documentation changes.

# When are we doing this?

We plan on rolling out these changes to developer.atlassian.com on Wednesday, December 9, 2015.

[comments-on-cac]: http://blogs.atlassian.com/2015/04/closing-comments-documentation/
[Atlassian Answers]: https://answers.atlassian.com/
