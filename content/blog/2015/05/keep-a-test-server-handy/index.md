---
title: "Tip of the Week: Keep a test server handy"
date: "2015-05-28T16:00:00+07:00"
author: "jgarcia"
categories: ["Server", "Ops"]
---

For Tip of the Week, we're taking suggestions from the community. This week's
tip comes to us from plugin developer [Daniel
Wester](https://twitter.com/dwester42a) of
[Wittified](http://www.wittified.com), who wants to remind us of how important
it is to test changes to your Atlassian applications before deploying them to production.

>For any of the products, it's important to have a staging/test instance
available to test major config changes and/or addons.
- Daniel Wester

## A friendly warning

Be careful to consider database and email settings when designing your test
environment. It's best to avoid copying the `application-home/dbconfig.xml` file
entirely; if this can't be avoided, be sure to modify its contents before
starting the test server. For JIRA, which uses mail handlers to fetch emails from
outside servers, it's important to disable this using the instructions at
[Restoring Data - Disable
Email](https://confluence.atlassian.com/display/JIRA/Restoring+Data#RestoringDat
    a-disableemail);
otherwise, a race condition can occur.

## Top five scenarios where a test server is handy

1. Testing upgrades to the main application product
2. Testing upgrades to Marketplace plugins
3. Developing and testing your own plugins, soy/velocity templates, and
integrations
4. Load testing the environment with Gatling or Selenium WebDriver
5. Simulating script results or configuration changes

Let us know in the comments below if there are any use cases you use that haven't been mentioned here.

Watch out for news and info from our
[@AtlassianDev](https://www.twitter.com/atlassiandev) Twitter feed!

Follow me at [@bitbucketeer](https://www.twitter.com/bitbucketeer)!
