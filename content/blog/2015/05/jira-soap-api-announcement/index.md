---
title: "JIRA SOAP API is waving goodbye"
date: "2015-05-08T06:00:00+07:00"
author: "dmeyer"
categories: ["JIRA"]
---

Remember February 2013? Pope Benedict XVI resigned, Beyoncé performed at the Super Bowl, and [Atlassian announced that JIRA's SOAP and XML-RPC APIs would be deprecated in JIRA 6.0.](https://developer.atlassian.com/jiradev/jira-apis/jira-api-status/jira-rpc-services/jira-xml-rpc-overview/soap-and-xml-rpc-api-deprecation-notice?utm_source=dac&utm_medium=blog&utm_campaign=jira-soap-api) Fast-forward two years and we're approaching the next major milestone in our journey: the SOAP and XML-RPC API modules _will be removed from JIRA Cloud soon and in the 7.0 release of JIRA Server_.

First, the good news. If you haven't migrated from SOAP to REST yet, we've [prepared a method-by-method migration guide to jumpstart your development](https://developer.atlassian.com/jiradev/jira-apis/jira-api-status/jira-rpc-services/jira-soap-to-rest-migration-guide?utm_source=dac&utm_medium=blog&utm_campaign=jira-soap-api).

Here are the key details:
* JIRA 7.0 will ship without the SOAP and XML-RPC APIs
* The JIRA REST API will have functional parity with SOAP
* JIRA's `rpc-soap` and `rpc-xmlrpc` modules will be removed
* JIRA will not ship with the `rpc-jira-plugin`

## Why did we do this?
Our original reasoning from February 2013 remains true:
> Deprecating SOAP and XML-RPC means that developers have a clear and consistent message from us about how they should be interacting with JIRA remotely. They won't build up code relying on SOAP and then be disappointed when new features don't get added or bugs don't get fixed. No false hope and no bullshit: REST is the future.

We've invested in adding functionality to the JIRA REST API while making it more usable and performant for developers. Atlassian has also introduced the [Atlassian Connect framework](https://developer.atlassian.com/static/connect/docs/latest/index.html?utm_source=dac&utm_medium=blog&utm_campaign=jira-soap-api), which has even more building blocks for rich, functional add-ons for JIRA that are scalable and cross-platform.

## What's next?
We know that many customers have written integrations with the SOAP API, or are using third-party add-ons that are built on top of the API. The JIRA Ecosystem team is committed to helping JIRA customers and add-on developers move to REST. We have prepared a [SOAP to REST migration guide](https://developer.atlassian.com/jiradev/jira-apis/jira-api-status/jira-rpc-services/jira-soap-to-rest-migration-guide?utm_source=dac&utm_medium=blog&utm_campaign=jira-soap-api) documenting the existing SOAP methods and our recommended REST equivalents.

We strongly encourage you to collaborate with us if you need help, need to report a bug, or have a feature request for the JIRA Ecosystem team. Simply [create an issue in the ACJIRA project on ecosystem.atlassian.net](https://ecosystem.atlassian.net/secure/CreateIssue!default.jspa?selectedProjectId=17771).

All the new 7.0 REST APIs will become available with the first EAP release of JIRA 7.0 Server and in JIRA Cloud in the next couple months. We will post another update here once this ships.