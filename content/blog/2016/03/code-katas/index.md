---
title: "Building your development skills with code katas"
date: "2016-03-16T16:00:00+07:00"
author: "tsmith"
categories: ["Katas"]
lede: "Code katas offer a method to learn and grow as a developer.
       But it takes some understanding of the practice to get real value from them."
---

For years I studied martial arts.
I did not spend my time studying the same martial art but branched out and studied
[Jujutsu], [Kempo], [Capoeira], [Kung fu], and [Taekwondo].
I started off with Taekwondo, and my learning involved repeating forms (or katas more generally)
until they became second nature.
I became frustrated by repeating katas again and again &mdash; which pushed me to branch out to other
martial arts.
It wasn't until later in my practice that I began to understand how repeating katas made me better.

While I haven't actively practiced martial arts in years now,
I have begun a new habit of doing code katas to refine my development craft.
Two to three times a week, I carve out 30 minutes for focused practice.

## What are code katas?

Code katas are simple exercises in programming used to hone your skills.
Often developers will repeat a given exercise each time, such as Roy Osherove's [string calculator kata].
Repeating this exercise provides the opportunity to hone your development skills with practice.

## What practice?

When starting with the Taekwondo forms, I struggled.
Repeating the same activities over and over without obvious purpose was boring.
It wasn't until I started doing more Jujutsu that I began to understand.
With Taekwondo, you would repeat the same form again and again.
The idea was to move the movements from active memory to muscle memory.
You shouldn't have to think about the act of blocking and attacking, it happens without thought.
In my practice of Jujutsu, katas were more like free-form sparring.
You might focus on a particular block, hold, or throw, but your partner was there to mix it up
so the input action was not always exactly the same.
This made it click with me, and I understood the purpose.

Starting off with code katas is similar.
At first, you might struggle to complete the exercise in a reasonable time box.
But once you figure out how to do that, repeating the exercise as is does not build important skills.
At this point you have to think about what you want to practice.

## Focus

When I first started with code katas, I focused on my tooling the most.
How can I run my unit tests the quickest?
How do I get feedback from my activities faster?
I would learn key combinations in my IDE for common actions.
I would create snippets for unit tests or other boilerplate I would write.
Whatever it takes to allow me to finish faster using my tools better.

After I felt comfortable with my tools, I focused elsewhere.
It would begin by something as simple as thinking "today I'm going to use library x"
and spend the time to learn the library as a part of my exercise.
The best part of doing it this way is that I already know the problem, so I can focus on the library I want to learn.
Each time I would add a constraint to my practice,
allowing me to expand in a different way and learn something new.

I'm not alone in [believing this is critical for katas to be useful][dont-kata].
Without focus, you might get better at solving the specific problem in the kata
but programming is about solving a range of problems.

## Getting started

I feel that doing code katas have benefited me as a developer.
It's not hard to get started. There's
[code katas in Coding Dojo's Kata Catalogue][kata-catalogue] &mdash; pick whatever interests you.
Carve out regular but limited time and get cranking.
Remember, with some of these katas at first you may not even be completing the exercise.
That is okay.
It's a process &mdash; it takes time to get beyond the problem of the exercise and on to focusing on a specific skill.

## What's next for me

I've used katas to help learn new languages and libraries.
Next, I want to find a way to do a full-stack web-app kata.
To this point all the katas I have practiced have focused on code and tooling.
The plentiful web frameworks that are out there could use a kata.
I imagine something like [TodoMVC](http://todomvc.com/),
but TodoMVC does not represent the type of web apps I commonly write.
I am on the lookout for a great, full-stack kata to take my practice to a new level.

[dont-kata]: https://hackhands.com/dont-code-katas/ "Why I don't do code katas"
[kata-catalogue]: http://codingdojo.org/cgi-bin/index.pl?KataCatalogue
[Jujutsu]: https://en.wikipedia.org/wiki/Jujutsu
[Kempo]: https://en.wikipedia.org/wiki/Kenp%C5%8D
[Capoeira]: https://en.wikipedia.org/wiki/Capoeira
[Kung fu]: https://en.wikipedia.org/wiki/Chinese_martial_arts
[Taekwondo]: https://en.wikipedia.org/wiki/Taekwondo
[string calculator kata]: http://osherove.com/tdd-kata-1/
