### Tips

- Restart everything with:

``bash
docker-compose --x-networking -f haproxy-demo.yml stop && \
docker-compose -f haproxy-demo.yml rm -f && \
docker rmi orchestration_nginx && \
docker-compose --x-networking -f haproxy-demo.yml up -d
``
`
