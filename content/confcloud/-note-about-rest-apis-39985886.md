---
title: Note About REST APIs 39985886
aliases:
    - /confcloud/-note-about-rest-apis-39985886.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39985886
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39985886
confluence_id: 39985886
platform:
product:
category:
subcategory:
---
# Confluence Connect : \_Note about REST APIs

The Confluence REST APIs are a prototype only

Confluence's REST APIs are evolving. Their functionality is currently limited to a subset of the existing Confluence API. We plan to improve the REST APIs in future releases. Please expect some API changes. If you decide to experiment with these APIs, we would welcome feedback. You can create an improvement request in the <a href="http://jira.atlassian.com/browse/CONF/component/13060" class="external-link">REST API component</a> of our <a href="http://jira.atlassian.com/secure/CreateIssue.jspa?pid=10470&amp;issuetype=4" class="external-link">JIRA project</a>.

For a production-ready API, consider using the [XML-RPC and SOAP APIs] or [JSON-RPC APIs]. These APIs support development using Javascript/AJAX or simple HTTP requests.

  [XML-RPC and SOAP APIs]: https://developer.atlassian.com/display/CONFDEV/Confluence+XML-RPC+and+SOAP+APIs
  [JSON-RPC APIs]: https://developer.atlassian.com/display/CONFDEV/Confluence+JSON-RPC+APIs

