---
title: 39984596
aliases:
    - /confcloud/39984596.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39984596
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39984596
confluence_id: 39984596
platform:
product:
category:
subcategory:
---
# Confluence Connect : What can I build with Confluence Connect?

Confluence Connect empowers you to build add-ons that improve content collaboration for teams every day.

 

Provide tools to help create and manage content:

-   Add new, specialised content within pages 
-   Provide tools to manage content, pages and spaces with Confluence

<img src="/confcloud/attachments/39984596/41226284.png" height="250" />

 

Integrate with or build on Confluence:

-   Integrate your service's content with Confluence to increase user engagement
-   Build custom content or features directly on the Confluence platform to enhance content collaboration

           <img src="/confcloud/attachments/39984596/39988754.png" height="250" />

 

 

------------------------------------------------------------------------

 

#### Confluence Connect provides patterns to make it straightforward to build your add-on, regardless of your product or business goal.

 

<table>
<colgroup>
<col width="33%" />
<col width="33%" />
<col width="33%" />
</colgroup>
<thead>
<tr class="header">
<th> </th>
<th><p>Add Content</p></th>
<th><p>Provide features to manage</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Add-On Categories</p></td>
<td><ul>
<li><strong>Diagramming:</strong> Specialised, rich content to visualise team activities</li>
<li><strong>Reports &amp; Charts:</strong> Data visualisations</li>
<li><strong>Content Layout:</strong> Control the formatting and layout of page content</li>
</ul></td>
<td><ul>
<li><strong>Workflows &amp; Communication:</strong>  Tools focused on team alignment and collaboration</li>
<li><strong>Content Management:</strong> Space-wide sharing and formatting of page content</li>
<li><strong>Community &amp; Engagement:</strong> Encourage active consumption and collaboration with team content</li>
</ul></td>
</tr>
<tr class="even">
<td>Patterns to Integrate with Confluence</td>
<td><p><a href="/confcloud/macros-39984560.html"><img src="/confcloud/attachments/39984596/39984589.png" class="confluence-thumbnail" width="85" /></a><a href="/confcloud/page-extensions-39985058.html"><img src="/confcloud/attachments/39984596/39984590.png" class="confluence-thumbnail" width="85" /></a><a href="/confcloud/search-extensions-39985059.html"><img src="/confcloud/attachments/39984596/39984591.png" class="confluence-thumbnail" width="85" /></a></p></td>
<td><p><a href="/confcloud/macros-39984560.html"><img src="/confcloud/attachments/39984596/39984589.png" class="confluence-thumbnail" width="85" /></a><a href="/confcloud/page-extensions-39985058.html"><img src="/confcloud/attachments/39984596/39984590.png" class="confluence-thumbnail" width="85" /></a></p></td>
</tr>
<tr class="odd">
<td>Patterns to Build with Confluence</td>
<td><p><a href="/confcloud/blueprints-39985061.html"><img src="/confcloud/attachments/39984596/39984587.png" class="confluence-thumbnail" width="85" /></a><a href="/confcloud/custom-content-39985060.html"><img src="/confcloud/attachments/39984596/39984588.png" class="confluence-thumbnail" width="85" /></a></p></td>
<td><p><a href="/confcloud/blueprints-39985061.html"><img src="/confcloud/attachments/39984596/39984587.png" class="confluence-thumbnail" width="85" /></a></p></td>
</tr>
</tbody>
</table>

 

## Where to from here?

-   Head back to [Confluence Connect Home]
-   See which [Confluence Connect patterns] are available
-   Dive in with the [getting started tutorial]
-   Read more about the [basics of Confluence Connect]
-   Read more about the [Atlassian Connect Framework]

  [<img src="/confcloud/attachments/39984596/39984589.png" class="confluence-thumbnail" width="85" />]: /confcloud/macros-39984560.html
  [<img src="/confcloud/attachments/39984596/39984590.png" class="confluence-thumbnail" width="85" />]: /confcloud/page-extensions-39985058.html
  [<img src="/confcloud/attachments/39984596/39984591.png" class="confluence-thumbnail" width="85" />]: /confcloud/search-extensions-39985059.html
  [<img src="/confcloud/attachments/39984596/39984587.png" class="confluence-thumbnail" width="85" />]: /confcloud/blueprints-39985061.html
  [<img src="/confcloud/attachments/39984596/39984588.png" class="confluence-thumbnail" width="85" />]: /confcloud/custom-content-39985060.html
  [Confluence Connect Home]: /confcloud/confluence-connect-development-39375836.html
  [Confluence Connect patterns]: /confcloud/confluence-connect-patterns-39981569.html
  [getting started tutorial]: /confcloud/introduction-to-confluence-connect-39985168.html
  [basics of Confluence Connect]: https://developer.atlassian.com/confcloud/what-is-confluence-connect
  [Atlassian Connect Framework]: https://developer.atlassian.com/static/connect/docs/latest/index.html

