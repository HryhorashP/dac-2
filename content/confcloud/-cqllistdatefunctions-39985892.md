---
title: Cqllistdatefunctions 39985892
aliases:
    - /confcloud/-cqllistdatefunctions-39985892.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39985892
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39985892
confluence_id: 39985892
platform:
product:
category:
subcategory:
---
# Confluence Connect : \_CQLListDateFunctions

-   [endOfDay()]
-   [endOfMonth()]
-   [endOfWeek()]
-   [endOfYear()]
-   [startOfDay()]
-   [startOfMonth()]
-   [startOfWeek()]
-   [startOfYear()]

  [endOfDay()]: https://developer.atlassian.com/display/CONFDEV/CQL+Function+Reference#CQLFunctionReference-endOfDay
  [endOfMonth()]: https://developer.atlassian.com/display/CONFDEV/CQL+Function+Reference#CQLFunctionReference-endOfMonth
  [endOfWeek()]: https://developer.atlassian.com/display/CONFDEV/CQL+Function+Reference#CQLFunctionReference-endOfWeek
  [endOfYear()]: https://developer.atlassian.com/display/CONFDEV/CQL+Function+Reference#CQLFunctionReference-endOfYear
  [startOfDay()]: https://developer.atlassian.com/display/CONFDEV/CQL+Function+Reference#CQLFunctionReference-startOfDay
  [startOfMonth()]: https://developer.atlassian.com/display/CONFDEV/CQL+Function+Reference#CQLFunctionReference-startOfMonth
  [startOfWeek()]: https://developer.atlassian.com/display/CONFDEV/CQL+Function+Reference#CQLFunctionReference-startOfWeek
  [startOfYear()]: https://developer.atlassian.com/display/CONFDEV/CQL+Function+Reference#CQLFunctionReference-startOfYear

