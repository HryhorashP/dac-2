---
title: Extending Page Actions 39988638
aliases:
    - /confcloud/extending-page-actions-39988638.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39988638
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39988638
confluence_id: 39988638
platform:
product:
category:
subcategory:
---
# Confluence Connect : Extending page actions

## Graphic elements required

None.

## Where can users find additional page actions?

Additional page actions should be stored under the more actions menu (<img src="/confcloud/attachments/39988638/44063040.png" class="confluence-thumbnail" width="22" />) on the top right hand of a page.

## UI components

Use [web items] to group page actions together in the more actions menu.

 

![]

## Recommendations

-   Keep similar actions together

  [web items]: https://developer.atlassian.com/static/connect/docs/latest/modules/common/web-item.html
  []: /confcloud/attachments/39988638/39988632.png

