---
title: Tutorials 39984583
aliases:
    - /confcloud/tutorials-39984583.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39984583
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39984583
confluence_id: 39984583
platform:
product:
category:
subcategory:
---
# Confluence Connect : Tutorials

We're creating more tutorials for Confluence Connect, and will add them here as soon as they're ready. Check this page regularly to try out the latest tutorials as we post them.

 

<table>
<colgroup>
<col width="25%" />
<col width="25%" />
<col width="25%" />
<col width="25%" />
</colgroup>
<thead>
<tr class="header">
<th><div class="tablesorter-header-inner">
Title
</div></th>
<th><div class="tablesorter-header-inner">
Description
</div></th>
<th><div class="tablesorter-header-inner">
Estimated Time
</div></th>
<th><div class="tablesorter-header-inner">
Level
</div></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><a href="https://developer.atlassian.com/display/CONFCLOUD/Development+setup">Development setup</a></td>
<td><p>Before undertaking any of our tutorials, this will help you set up your development environment.</p></td>
<td>15 minutes</td>
<td><div class="content-wrapper">
0 - SETUP
</div></td>
</tr>
<tr class="even">
<td><a href="https://developer.atlassian.com/display/CONFCLOUD/Custom+Content+with+Confluence+Connect">Custom Content</a></td>
<td>An advanced walk-through of custom content in Confluence.</td>
<td>60 minutes</td>
<td><div class="content-wrapper">
<p>3 - INTERMEDIATE</p>
</div></td>
</tr>
<tr class="odd">
<td><a href="https://developer.atlassian.com/display/CONFCLOUD/Quick+start+to+Confluence+Connect">Quick start to Confluence Connect</a></td>
<td>An accelerated guide to building a basic &quot;hello world&quot; macro using Confluence Connect.</td>
<td>10 minutes</td>
<td><div class="content-wrapper">
1 - QUICK START
</div></td>
</tr>
<tr class="even">
<td><a href="https://developer.atlassian.com/display/CONFCLOUD/Macro+Autoconvert+with+Confluence+Connect">Macro Autoconvert</a></td>
<td>Add autoconvert patterns to your macro experience.</td>
<td>15 minutes</td>
<td><div class="content-wrapper">
<p>3 - INTERMEDIATE</p>
</div></td>
</tr>
<tr class="odd">
<td><a href="https://developer.atlassian.com/display/CONFCLOUD/Macro+Custom+Editor+with+Confluence+Connect">Macro Custom Editor</a></td>
<td>Add a custom editor to your macro experience.</td>
<td>15 minutes</td>
<td><div class="content-wrapper">
<p>3 - INTERMEDIATE</p>
</div></td>
</tr>
<tr class="even">
<td><a href="https://developer.atlassian.com/display/CONFCLOUD/Content+Byline+Items+with+Confluence+Connect">Content Byline Items</a></td>
<td>A step-by-step introduction to building a Content Byline item in Confluence</td>
<td>25 minutes</td>
<td><div class="content-wrapper">
2 - BEGINNER
</div></td>
</tr>
<tr class="odd">
<td><a href="https://developer.atlassian.com/display/CONFCLOUD/Introduction+to+Confluence+Connect">Intro to Confluence Connect</a></td>
<td>A step-by-step introduction to building a basic Confluence add-on and macro.</td>
<td>45 minutes</td>
<td><div class="content-wrapper">
2 - BEGINNER
</div></td>
</tr>
<tr class="even">
<td><a href="https://developer.atlassian.com/display/CONFCLOUD/Multi-page+Blueprints+with+Confluence+Connect">Multi-page Blueprints</a></td>
<td><p>A quick guide to building a multi-page blueprint with a custom parent and child pages.</p></td>
<td><p>60 minutes</p></td>
<td><p>3 - INTERMEDIATE</p></td>
</tr>
<tr class="odd">
<td><a href="/confcloud/theming-with-confluence-connect-44063934.html">Theming</a></td>
<td>Change the look and feel of all content, or content within a specific space.</td>
<td>15 minutes</td>
<td>3 - INTERMEDIATE</td>
</tr>
</tbody>
</table>

  [Development setup]: https://developer.atlassian.com/display/CONFCLOUD/Development+setup
  [Custom Content]: https://developer.atlassian.com/display/CONFCLOUD/Custom+Content+with+Confluence+Connect
  [Quick start to Confluence Connect]: https://developer.atlassian.com/display/CONFCLOUD/Quick+start+to+Confluence+Connect
  [Macro Autoconvert]: https://developer.atlassian.com/display/CONFCLOUD/Macro+Autoconvert+with+Confluence+Connect
  [Macro Custom Editor]: https://developer.atlassian.com/display/CONFCLOUD/Macro+Custom+Editor+with+Confluence+Connect
  [Content Byline Items]: https://developer.atlassian.com/display/CONFCLOUD/Content+Byline+Items+with+Confluence+Connect
  [Intro to Confluence Connect]: https://developer.atlassian.com/display/CONFCLOUD/Introduction+to+Confluence+Connect
  [Multi-page Blueprints]: https://developer.atlassian.com/display/CONFCLOUD/Multi-page+Blueprints+with+Confluence+Connect
  [Theming]: /confcloud/theming-with-confluence-connect-44063934.html

