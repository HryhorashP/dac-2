<div class="patterns">
  <h3 class="title">Example add-ons</h3>

  <h2 class="subtitle">Java</h2>
  <a href="https://bitbucket.org/atlassianlabs/atlassian-autowatch-plugin">JIRA Autowatcher</a>
  <p>Automatically add watchers to newly created JIRA's</p>
  <a href="https://bitbucket.org/atlassianlabs/webhook-inspector">Webhook Inspector</a>
  <p>Inspects the response bodies of the available webhooks in Atlassian Connect.</p>
  <div class="large-2">
      <hr>
  </div>

  <h2 class="subtitle">Node.JS</h2>
  <a href="https://bitbucket.org/robertmassaioli/ep-tool">Entity property tool</a>
  <p>An Atlassian Connect add-on that allows manipulation of entity properties in JIRA.</p> 
  <a href="https://bitbucket.org/atlassian/whoslooking-connect/overview">Who's Looking</a>
  <p>Displays the users who are currently looking at a JIRA issue.</p>
  <div class="large-2">
      <hr>
  </div>
  <h2 class="subtitle">Haskell</h2>
  <a href="https://bitbucket.org/robertmassaioli/hackathon">Hackathon</a>
  <p>Run any Hackathon via JIRA with this feature rich sample integration.This is 
  the add-on Atlassian uses for Shilt</p> 
  <a href="https://bitbucket.org/atlassianlabs/my-reminders/overview">My reminders</a>
  <p>Sends you reminders about JIRA issues</p>
  <div class="large-2">
      <hr>
  </div>
</div>
