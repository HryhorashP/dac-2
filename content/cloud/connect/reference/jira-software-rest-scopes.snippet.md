# JIRA Software REST scopes
For more information about the JIRA Software REST APIs, please refer to [the documentation on docs.atlassian.com](https://developer.atlassian.com/jiradev/jira-apis).

{{< include path="content/cloud/connect/reference/product-api-scopes.snippet.md" >}}