---
title: "Authentication for add-ons"
platform: cloud
product: jiracloud
category: devguide
subcategory: security
aliases:
    - /cloud/jira/platform/authentication-for-add-ons.html
    - /cloud/jira/platform/authentication-for-add-ons.md
date: "2016-10-31"
---
{{< include path="content/cloud/connect/concepts/authentication.snippet.md" >}}