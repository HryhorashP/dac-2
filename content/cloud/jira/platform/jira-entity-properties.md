---
title: JIRA Entity Properties 
platform: cloud
product: jiracloud
category: devguide
subcategory: blocks
aliases:
- /jiracloud/jira-entity-properties-39988397.html
- /jiracloud/jira-entity-properties-39988397.md
confluence_id: 39988397
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39988397
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39988397
date: "2016-09-14"
---
# Entity properties

Entity properties allow add-ons to add key/value stores to JIRA entities, such as issues or projects. These values can be indexed by JIRA and queried via a REST API or through JQL. This page provides an overview and examples of how entity properties work.

**On this page:**
-   [How do I store data against an entity using REST?]
    -   [Example 1: Storing data]
    -   [Example 2: Retrieving data]
    -   [Example 3: Removing a property]
-   [How do I make the properties of an entity searchable?]

 
## How do I store data against an entity using REST?

The <a href="http://curl.haxx.se/" class="external-link">curl</a>examples below show you how you can store data against an issue and retrieve the data from an issue, using REST. 

-   For information how to manipulate properties of other JIRA entities, e.g. projects, please see the <a href="/cloud/jira/platform/jira-cloud-platform-rest-api/" class="external-link">JIRA REST API documentation</a>. 
-   P2 plugins can store/retrieve data using <a href="https://docs.atlassian.com/jira/latest/com/atlassian/jira/bc/issue/properties/IssuePropertyService.html" class="external-link">IssuePropertyService</a>.

Please note, in order to modify, add or remove the properties, the user who is executing the request must have permission to edit the entity. For example, to add new property to issue ENPR-4, you need permission to edit the issue. In order to retrieve a property, the user must have read permissions for the entity.

#### Example 1: Storing data

The simple example below will store the JSON object **{"content":"Test if works on JIRA Cloud", "completed" : 1}** against issue ENPR-4 with the key **tasks. **

``` bash
curl -X PUT -H "Content-type: application/json" https://jira-instance1.net/rest/api/2/issue/ENPR-4/properties/tasks -d '{"content":"Test if works on JIRA Cloud", "completed" : 1}'
```

Note:

-   The **key** has a maximum length of 255 characters. 
-   The **value** must be a valid JSON Object and has a maximum size of 32 KB.

#### Example 2: Retrieving data

The example below shows how to get all of the properties stored against an issue.

``` bash
curl -X GET https://jira-instance1.net/rest/api/2/issue/ENPR-4/properties/
```

The response from server will contain keys and URLs of all properties of the issue ENPR-4. 

``` js
{"keys":[{"self":"https://jira-instance1.net/rest/api/2/issue/ENPR-4/properties/tasks","key":"tasks"}]}
```

#### Example 3: Removing a property

The example below shows how to remove a property from an issue.

``` bash
curl -X DELETE https://jira-instance1.net/rest/api/2/issue/ENPR-4/properties/tasks
```

## How do I make the properties of an entity searchable?

Atlassian Connect and P2 plugins both can provide a module descriptor, which will make the issue properties searchable using JQL. For example, to index the data from the first example, P2 and Connect plugins can provide the following module descriptors:

**P2 module descriptor**

``` xml
<index-document-configuration 
  entity-key="IssueProperty" 
  key="jira-issue-tasklist-indexing">
  <key property-key="tasks">
    <extract path="content" type="text" />
  </key>
</index-document-configuration>
```

 

 

**Connect module**

``` js
"jiraEntityProperties": [{
    "name": {
        "value" :"Tasks index",
        "i18n": "tasks.index"
    },
    "entityType": "issue",
    "keyConfigurations": [{
        "propertyKey" : "tasks",
        "extractions" : [{
            "objectName" : "content",
            "type" : "text"
        }]
    }]
}]
```

The descriptors shown above will make JIRA index the object **"content"** of the issue property **"tasks"** as a text. The available index data types are:

-   `number`: indexed as a number and allows for range ordering and searching on this field.
-   `text:` tokenized before indexing and allows for searching for particular words.
-   `string:` indexed as-is and allows for searching for the exact phrase only.
-   `date:` indexed as a date and allows for date range searching and ordering. The expected date format is \[YYYY\]-\[MM\]-\[DD\]. The expected date time format is \[YYYY\]-\[MM\]-\[DD\]T\[hh\]:\[mm\]:\[ss\] with optional offset from UTC: +/-\[hh\]:\[mm\] or `Z` for no offset. For reference, please see <a href="http://www.w3.org/TR/NOTE-datetime" class="external-link">ISO_8601 standard</a>.

The indexed data is available for a JQL search. The result of the JQL query: *issue.property\[tasks\].completed = 1 AND issue.property\[tasks\].content ~ "works" *is shown in the screenshot below. 

![Alt text](../../platform/images/jira-search-entityproperties.png)

Please see the [Connect documentation] for detailed explanations on how to use the module descriptors. 

  [How do I store data against an entity using REST?]: #how-do-i-store-data-against-an-entity-using-rest
  [Example 1: Storing data]: #example-1-storing-data
  [Example 2: Retrieving data]: #example-2-retrieving-data
  [Example 3: Removing a property]: #example-3-removing-a-property
  [How do I make the properties of an entity searchable?]: #how-do-i-make-the-properties-of-an-entity-searchable
  [Connect documentation]: https://developer.atlassian.com/static/connect/docs/modules/jira/entity-property.html
