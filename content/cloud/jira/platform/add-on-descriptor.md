---
aliases:
- /jiracloud/add-on-descriptor.html
- /jiracloud/add-on-descriptor.md
category: devguide
platform: cloud
product: jiracloud
subcategory: blocks
title: "Add-on descriptor"
---

{{< include path="content/cloud/jira/platform/temp/add-on-descriptor.snippet.md" >}}

{{< include path="content/cloud/connect/reference/descriptor-schemas.snippet.md">}}

{{< include path="content/cloud/jira/platform/temp/add-on-descriptor-reference.snippet.md" >}}

{{< include path="content/cloud/jira/platform/temp/authentication.snippet.md" >}}

{{< include path="content/cloud/jira/platform/temp/lifecycle.snippet.md" >}}

{{< include path="content/cloud/jira/platform/temp/add-on-vendor.snippet.md" >}}