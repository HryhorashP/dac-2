---
title: Hosted Data Storage
platform: cloud
product: jsdcloud
category: devguide
subcategory: block
aliases:
- /cloud/jira/service-desk/hosted-data-storage.html
- /cloud/jira/service-desk/hosted-data-storage.md
---
{{< include path="content/cloud/connect/concepts/hosted-data-storage.snippet.md">}}
