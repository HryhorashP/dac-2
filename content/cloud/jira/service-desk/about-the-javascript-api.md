---
title: About the JavaScript API
platform: cloud
product: jsdcloud
category: reference
subcategory: jsapi
date: "2016-11-01"
---
{{< include path="content/cloud/connect/concepts/javascript-api.snippet.md">}}