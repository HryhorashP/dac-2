<h2>Join an Atlassian User Group</h2>

Build better together. Be part of a thriving community of developers, and engineers, and product managers. <br> <br><a href="http://aug.atlassian.com">Start connecting <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
